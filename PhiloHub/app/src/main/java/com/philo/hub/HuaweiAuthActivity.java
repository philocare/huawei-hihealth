package com.philo.hub;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import com.huawei.hihealth.error.HiHealthError;
import com.huawei.hihealthkit.auth.HiHealthAuth;
import com.huawei.hihealthkit.auth.HiHealthOpenPermissionType;
import com.huawei.hihealthkit.auth.IAuthorizationListener;

public class HuaweiAuthActivity extends AppCompatActivity {

    private static final String TAG = "HuaweiAuthActivity";

    // arquivo e flag de log
    private String pathConfig = "/philocare/";
    private String pathLog = "/philocare/log/";
    private String configFileName = "config.cnf";

    private Button buttonRequestAuthorization;
    private static final int REQUEST_AUTHORIZATION = 0;

    // variáveis para o detector de Bluetooth (dispositivos)
    private BluetoothManager btManager;
    private BluetoothAdapter btAdapter;
    private BluetoothLeScanner btScanner;
    private final static int REQUEST_ENABLE_BT = 1;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        /*getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }*/

        buttonRequestAuthorization = findViewById(R.id.btnRequestAuthorization);

        Toolbar toolbar = (Toolbar) findViewById(R.id.settings_toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                HuaweiAuthActivity.super.onBackPressed();
            }
        });

    }

    //#############################################################################################
    // Funções executadas depois do oncreate
    //#############################################################################################
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume () {
        super.onResume();
        // usaremos a função getGender para verificar se estamos conseguido ler o Huawei
        //SysUtils..checkConnection(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }


    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();

    }

    /*
    ################################################################################################
    Funções para click dos botões
    ################################################################################################
     */

    public void btnRequestAutorizationClick(View view) {
        doRequestAuthorization(this);
    }

    /*
    ################################################################################################
    Função da SDK Huawei para pedir autorização para o aplicativo
    ################################################################################################
     */

    /**
     * Developer Alliance Code:requestAuthorization
     *
     * @param context context
     */
    private void doRequestAuthorization(Context context) {
        int[] read = new int[] {
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_USER_PROFILE_INFORMATION,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_USER_PROFILE_FEATURE,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_DATA_SET_HEART,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_DATA_SET_WALK_METADATA,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_DATA_SET_RUN_METADATA,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_DATA_SET_RIDE_METADATA,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_DATA_SET_WEIGHT,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_DATA_SET_CORE_SLEEP,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_DATA_POINT_STEP_SUM,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_DATA_POINT_DISTANCE_SUM,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_DATA_POINT_INTENSITY,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_DATA_POINT_CALORIES_SUM,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_REALTIME_HEARTRATE,
                HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_READ_DATA_REAL_TIME_SPORT
        };
        //int[] write = new int[] {HiHealthOpenPermissionType.HEALTH_OPEN_PERMISSION_TYPE_WRITE_DATA_SET_WEIGHT};
        HiHealthAuth.requestAuthorization(context, null, read, new IAuthorizationListener() {
            @Override
            public void onResult(int resultCode, Object resultDesc) {
                Log.i(TAG, "requestAuthorization onResult:" + resultCode);
                Message message = Message.obtain();
                message.what = REQUEST_AUTHORIZATION;
                message.obj = resultDesc;
                //demoHandler.sendMessage(message);
                switch (resultCode) {
                    case HiHealthError.SUCCESS:
                        Log.i(TAG, "Authorization page is shown foreground.");
                        break;
                    case HiHealthError.FAILED:
                        Log.i(TAG, "AIDL connection failed.");
                        break;
                    case HiHealthError.ERR_SCOPE_EXCEPTION:
                        Log.i(TAG, "Contact developer website for more info.");
                        break;
                    case HiHealthError.ERR_API_EXECEPTION:
                        Log.i(TAG, "Try update Health app or try later.");
                        break;
                    default:
                        Log.i(TAG, "Please contact HiHealth team if you catch this");
                }
            }
        });
    }



}