package com.philo.hub;


import android.content.Context;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.widget.TextView;

import com.huawei.hihealthkit.data.HiHealthPointData;
import com.huawei.hihealthkit.data.HiHealthSetData;

import org.apache.commons.io.comparator.LastModifiedFileComparator;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class SysUtils {

	public static final String NOT_FOUND = "NOT_FOUND";
	public static String pathConfig = "/philocare/";
	public static String configFileName = "config.cnf";
	public static String dataHRFileName = "dataHR.txt";
	public static String dataSleepFileName = "dataSleep.txt";
	public static String dataHealthFileName5min = "dataHealth5min.txt";
	public static String dataHealthFileName30min = "dataHealth30min.txt";
	public static String lastDateFileName = "lastHRDate.inf";
	public static String dataHealthFileName = "dataHealth";
	public static String file_extension = ".txt";
	public static int DAYS = 3;
	public static int MINUTES_IN_3_DAYS = 4320;
	public static int WATCH_DAYS = 4320;

	// Line separators for the display on the UI
	private static final String SPLIT = "*******************************" + System.lineSeparator();

	public static boolean deleteFile(String filename) {

		// Encontra o diretório de arquivos
		File sdcardDir = Environment.getExternalStorageDirectory();
		String path = sdcardDir.getPath() + pathConfig;
		// pega a lista de arquivos

		File fdelete = new File(path, filename);
		if (fdelete.exists()) {
			return fdelete.delete();
		} else {
			return false;
		}
	}

	public static String[] readTxtFile() {

		String[] result = new String[2];
		// Encontra o diretório de arquivos
		File sdcardDir = Environment.getExternalStorageDirectory();
		String path = sdcardDir.getPath() + pathConfig;
		// pega a lista de arquivos
		File directory = new File(path);
		File[] files = directory.listFiles();

		// verifica se tem arquivos
		if (files.length == 0) {
			// nenhum arquivo para enviar
			return null;
		}

		// Ordena para a mais antiga
		Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_COMPARATOR);

		//Get the first text file
		File file = new File(path,files[0].getName());

		result[0] = files[0].getName();

		//Read text from file
		StringBuilder text = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				text.append(line);
				text.append('\n');
			}
			br.close();
		}
		catch (IOException e) {
			//You'll need to add proper error handling here
			return null;
		}

		result[1] = text.toString();

		return result;
	}

	//Gravar string no arquivo de texto
	public static void writeTxtToFile(String devicecode, String strcontent, int index, boolean overwrite) {
		writeTxtToFile(devicecode, strcontent, pathConfig, dataHealthFileName + index +  file_extension, overwrite);
	}

	//Gravar string no arquivo de texto
	public static void writeTxtToFile(String devicecode, String strcontent, boolean overwrite) {
		writeTxtToFile(devicecode, strcontent, pathConfig, dataHealthFileName +  file_extension, overwrite);
	}

	//Gravar string no arquivo de texto
	public static void writeTxtToFile(String devicecode, String strcontent, String last_date, boolean overwrite) {
		writeTxtToFile(devicecode, strcontent, pathConfig, dataHealthFileName + last_date +  file_extension, overwrite);
	}


	//Gravar string no arquivo de texto
	public static void writeTxtToFile(String devicecode, String strcontent, String last_date, boolean overwrite, String dtype) {
		writeTxtToFile(devicecode, strcontent, pathConfig, dataHealthFileName + "_" + last_date + dtype + file_extension, overwrite);
	}

	//Gravar string no arquivo de texto
	public static void writeTxtToFile(String devicecode, String strcontent, String filePath, String fileName, boolean overwrite) {
		// Após gerar a pasta, gere o arquivo, caso contrário, ocorrerá um erro
		File sdcardDir = Environment.getExternalStorageDirectory();
		String path = sdcardDir.getPath() + filePath;
		Calendar today = Calendar.getInstance();
		fileName = today.get(Calendar.YEAR) + "-" + (today.get(Calendar.MONTH) + 1) + "-" + today.get(Calendar.DAY_OF_MONTH) + "-" + fileName;
		makeFilePath(path, fileName);

		String strFilePath = path + fileName;
		//Cada vez que você escreve, escreva em uma nova linha
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String now = sdf.format(new Date());
		String strContent;
		strContent = "@" + devicecode + "@" + now + "@" + strcontent;

		try {
			File file = new File(strFilePath);
			if (!file.exists()) {
				Log.d("TestFile", "Create the file:" + strFilePath);
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			RandomAccessFile raf = new RandomAccessFile(file, "rwd");
			if (!overwrite) {
				raf.seek(file.length());
			}
			raf.write(strContent.getBytes());
			raf.close();
		} catch (Exception e) {
			Log.e("writeTxtToFile", "Error on write File:" + e);
		}
	}

	// Gerar arquivo
	public static File makeFilePath(String filePath, String fileName) {
		File file = null;
		makeRootDirectory(filePath);
		try {
			file = new File(filePath + fileName);
			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}

	//Gerar pasta
	public static void makeRootDirectory(String filePath) {
		File file = null;
		try {
			file = new File(filePath);
			if (!file.exists()) {
				file.mkdir();
			}
		} catch (Exception e) {
			Log.i("error:", e+"");
		}
	}

	public static String printHexString(byte[] b) {
		if(b == null) {
			return "";
		}

		String hexString = "";
		for (int i = 0; i < b.length; i++) {
			String hex = Integer.toHexString(b[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			hexString += hex.toUpperCase() + " ";
		}

		return hexString;
	}

	/*
    Recupera o nome e código do dispositivo salvo no arquivo de preferências
    */
	public static String getSavedDevice (Context context) {

		String ret = "";
		String name_code = "";
		char[] buf = new char[256];

		try{
			File file = new File(context.getFilesDir() + pathConfig);
			File gpxfile = new File(file, configFileName);
			FileReader reader = new FileReader(gpxfile);
			reader.read(buf);
			reader.close();

			name_code = new String(buf);

			name_code = name_code.trim();

		}catch (Exception e){
			e.printStackTrace();

		}

		return name_code;
	}

	/*
	Salva o nome e código da pulseira no arquivo de preferências
 	*/
	public static void saveDevice (Context context, String code) throws IOException {

		try{
			File file = new File(context.getFilesDir() + pathConfig);
			if(!file.exists()){
				file.mkdir();
			}
			// por enquanto vamos fazer apenas de um dispositivo
			File gpxfile = new File(file, configFileName);
			FileWriter writer = new FileWriter(gpxfile);
			writer.write(code);
			writer.flush();
			writer.close();

		}catch (Exception e){
			e.printStackTrace();

		}
	}

	/*
	Salva a última data em arquivos de configuração
	 */
	public static void saveInfoData(Context context, String info, String file_extension) throws IOException {

		try{
			File file = new File(context.getFilesDir() + pathConfig);
			if(!file.exists()){
				file.mkdir();
			}
			// por enquanto vamos fazer apenas de um dispositivo
			File gpxfile = new File(file, lastDateFileName + "_" + file_extension);
			FileWriter writer = new FileWriter(gpxfile);
			writer.write(info);
			writer.flush();
			writer.close();

		}catch (Exception e){
			e.printStackTrace();

		}
	}

	/*
Recupera o nome e código do dispositivo salvo no arquivo de preferências
*/
	public static String loadInfoData(Context context, String file_extension) {

		String ret = "";
		String info_data = "";
		char[] buf = new char[256];

		try{
			File file = new File(context.getFilesDir() + pathConfig);
			File gpxfile = new File(file, lastDateFileName + "_" + file_extension);
			if (file.exists()) {
				FileReader reader = new FileReader(gpxfile);
				reader.read(buf);
				reader.close();
				info_data = new String(buf);
				info_data = info_data.trim();
			} else {
				info_data = NOT_FOUND;
			}

		}catch (Exception e){
			e.printStackTrace();

		}

		return info_data;
	}

	public static void doSendFiles(Context context)
	{
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		File sdcardDir = Environment.getExternalStorageDirectory();
		String path = sdcardDir.getPath() + "/philocare/";

		File directory = new File(path);
		File[] files = directory.listFiles();

		if (files != null) {
			// envia todos arquivos no dir philocare para o servidor
			for (File file : files) {
				StringBuffer sb = new StringBuffer("ftp://philo:7UT851@philocare.com/public_html/philocare.com/engine/huawei/" + file.getName());
				sb.append(";type=a");
				BufferedInputStream bis = null;
				BufferedOutputStream bos = null;
				try {
					URL url = new URL(sb.toString());
					URLConnection urlc = url.openConnection();

					bos = new BufferedOutputStream(urlc.getOutputStream());
					String filename = path + file.getName();
					bis = new BufferedInputStream(new FileInputStream(filename));

					int i;
					// read byte by byte until end of stream
					while ((i = bis.read()) != -1) {
						bos.write(i);
					}
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					// finalizou o envio, fecha as conexões
					if (bis != null)
						try {
							bis.close();
						} catch (IOException ioe) {
							ioe.printStackTrace();
						}
					if (bos != null)
						try {
							bos.close();
						} catch (IOException ioe) {
							ioe.printStackTrace();
						}
					// apaga o arquivo enviado
					file.delete();
				}
			}
		}
	}

	/**
	 * Printout failure exception error code and error message
	 *
	 * @param  tag activity log tag
	 * @param e   Exception object
	 * @param api Interface name
	 * @param logInfoView  Text View object
	 */
	/*static void printFailureMessage(String tag, Exception e, String api, TextView logInfoView) {
		String errorCode = e.getMessage();
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(errorCode);
		if (e instanceof ApiException) {
			int eCode = ((ApiException) e).getStatusCode();
			String errorMsg = HiHealthStatusCodes.getStatusCodeMessage(eCode);
			logger(api + " failure " + eCode + ":" + errorMsg, tag, logInfoView);
			return;
		} else if (isNum.matches()) {
			String errorMsg = HiHealthStatusCodes.getStatusCodeMessage(Integer.parseInt(errorCode));
			logger(api + " failure " + errorCode + ":" + errorMsg, tag, logInfoView);
			return;
		} else {
			logger(api + " failure " + errorCode, tag, logInfoView);
		}
		logger(SPLIT, tag, logInfoView);
	}*/

	/**
	 * Send the operation result logs to the logcat and TextView control on the UI
	 *
	 * @param string indicating the log string
	 * @param  tag activity log tag
	 * @param logInfoView  Text View object
	 */
	static void logger(String string, String tag, TextView logInfoView) {
		Log.i(tag, string);
		// colocar timestamp
		Date now = new Date();
		string = now.toString() + string;
		logInfoView.append(string + System.lineSeparator());
		int offset = logInfoView.getLineCount() * logInfoView.getLineHeight();
		if (offset > logInfoView.getHeight()) {
			logInfoView.scrollTo(0, offset - logInfoView.getHeight());
		}
	}

	/**
	 * Build the json to send to server.
	 *
	 * @param sampleSet (indicating the sampling dataset)
	 */
	public static String buildJSONSampleSet(List sampleSet, int datatype) {
		// grava em arquivo para ser enviado para o servidor
		// transformar em JSON - Gson from Google
		//String json = gson.toJson(sampleSet);
		//logger("Json:" + json);
		String result = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (sampleSet.size() > 0) {
			for (Object obj : sampleSet) {
				if ((datatype == 40002) || (datatype == 40004)) {
					HiHealthPointData hiHealthDatas = (HiHealthPointData) obj;
					result = result + "{";
					result = result + "\"type\":\"" + hiHealthDatas.getType() + "\",";
					result = result + "\"starttime\":\"" + dateFormat.format(new Date(hiHealthDatas.getStartTime())) + "\",";
					result = result + "\"endtime\":\"" + dateFormat.format(new Date(hiHealthDatas.getEndTime())) + "\",";
					result = result + "\"value\":\"" + hiHealthDatas.getValue() + "\"},";
				} else {
					HiHealthSetData hiHealthDatas = (HiHealthSetData) obj;
					result = result + "{";
					result = result + "\"type\":\"" + hiHealthDatas.getType() + "\",";
					result = result + "\"starttime\":\"" + dateFormat.format(new Date(hiHealthDatas.getStartTime())) + "\",";
					result = result + "\"endtime\":\"" + dateFormat.format(new Date(hiHealthDatas.getEndTime())) + "\",";
					// valor
					Map map = hiHealthDatas.getMap();
					for (Object key : map.keySet()) {
						// colocar timestamp nos dados
						result = result + "\"key\":\"" + key + "\",";
						Object value = map.get(key);
						result = result + "\"value\":\"" + value + "\",";
					}
					// Retira a ultima virgula
					result = result.substring(0, result.length() - 1);
					result = result + "},";
				}
			}
		} else {
			result = "{NO DATA}";
		}

		if (result.length() > 0) {
			// Retira a ultima virgula
			result = result.substring(0, result.length() - 1);
		}
		return result;
	}
}
