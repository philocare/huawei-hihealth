/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.philo.hub;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.huawei.hihealth.listener.ResultCallback;
import com.huawei.hihealthkit.HiHealthDataQuery;
import com.huawei.hihealthkit.HiHealthDataQueryOption;
import com.huawei.hihealthkit.data.HiHealthPointData;
import com.huawei.hihealthkit.data.HiHealthSetData;
import com.huawei.hihealthkit.data.store.HiHealthDataStore;
import com.huawei.hihealthkit.data.type.HiHealthPointType;
import com.huawei.hihealthkit.data.type.HiHealthSetType;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class HuaweiDataColectorToMemory {

    public static int DataCount = 0;

    /**
     * Tipo: 10007
     * 44209 TOTAL DE MINUTOS DORMIDOS
     * 44101 TOTAL DE MINUTOS DE SONO REM
     * 44102 TOTAL DE MINUTOS DE SONO PROFUNDO
     * 44103 TOTAL DE MINUTOS DE SONO LEVE
     * 44105 MINUTOS DO SONO DO DIA TODO
     * 44201 HORA DE DEITAR/DORMIR
     * 44106 MAX DURAÇÃO SONO PROFUNDO
     * 44202 HORA DE ACORDAR/LEVANTAR
     * 44107 NÚMERO DE VEZES QUE LEVANTOU DURANTE O SONO
     * 44203 PONTUAÇÃO DO SONO CALCULADA PELA HUAWEI
     *
     * Tipo: 10008
     * 46016 BATIMENTO MÁXIMO NO DIA
     * 46017 BATIMENTO MINIMO NO DIA
     * 46018 BATIMENTO DE REPOUSO NO DIA
     *
     * Tipo: 40002 - value = TOTAL DE PASSOS NO DIA
     */
    static class HuaweiDataClass {
        public Date timestamp;
        public int total_sleep;
        int total_sleep_rem;
        int total_sleep_deep;
        int total_sleep_ligh;
        int total_sleep_day;
        Date sleep_bed_timestamp;
        int max_deep_sleep_time;
        Date sleep_rise_timestamp;
        int sleep_awake_times;
        int sleep_score;
        int max_heart_rate;
        int min_heart_rate;
        int rest_heart_rate;
        int num_steps;
    }

    private static final String TAG = "HuaweiDataColectorToMemory";
    private static final String SPLIT = "*******************************" + System.lineSeparator();

    // TextView for displaying operation information on the UI
    private static TextView logInfoView;

    // Object of controller for fitness and health data, providing APIs for read/write, batch read/write, and listening
    //private static DataController dataController;

    private static String last_last_date = "";

    private static ArrayList<HuaweiDataClass> DataBuffer;

    // o fim é o dia de hoje
    public static long endTime = 0;
    // o ultimo dia extraido
    public static long startTime = 0;

    /**
     * TextView to send the operation result logs to the logcat and to the UI
     *
     * @param string (indicating the log string)
     */
    private static void logger(String string) {
        SysUtils.logger(string, TAG, logInfoView);
    }

    /**
     * Inicializa o data controler de extração
     * @param ctx - Contexto da aplicação
     * @param txtView - Objeto de exibição
     */
    public static void initDataController(Context ctx, TextView txtView ) {
        DataBuffer = new ArrayList<>();
        logInfoView = txtView;
    }

    public static void getHealthData(Context ctx) throws ParseException, IOException {

        // Limpa o buffer
        DataBuffer.clear();
        DataCount = 0;
        //getDataFromHuawei(ctx, HiHealthSetType.DATA_SET_WALK_METADATA, false);
        //getDataFromHuawei(ctx, HiHealthSetType.DATA_SET_RUN_METADATA, false);
        //getDataFromHuawei(ctx, HiHealthSetType.DATA_SET_RIDE_METADATA, false);
        //getDataFromHuawei(ctx, HiHealthSetType.DATA_SET_WEIGHT_EX, false);

        getDataFromHuawei(ctx, HiHealthSetType.DATA_SET_CORE_SLEEP, false);
        getDataFromHuawei(ctx, HiHealthSetType.DATA_SET_HEART, false);
        getDataFromHuawei(ctx, HiHealthPointType.DATA_POINT_STEP_SUM, false);

        //getDataFromHuawei(ctx, HiHealthPointType.DATA_POINT_REST_HEARTRATE, false);
        //getDataFromHuawei(ctx, HiHealthPointType.DATA_POINT_HEALTH_MIN, false);
        //getDataFromHuawei(ctx, HiHealthPointType.DATA_POINT_LAST_OXYGEN_SATURATION, false);
        //getDataFromHuawei(ctx, HiHealthPointType.DATA_POINT_DISTANCE_SUM, false);
        //getDataFromHuawei(ctx, HiHealthPointType.DATA_POINT_SLEEP_DURATION_SUM, false);
        //getDataFromHuawei(ctx, HiHealthPointType.DATA_POINT_SLEEP_WAKE_COUNT, false);
        //getDataFromHuawei(ctx, HiHealthPointType.DATA_POINT_MIN, false);
        //getDataFromHuawei(ctx, HiHealthPointType.DATA_SET_MIN, false);
    }

    /**
     * lê dados da Huawei e salva em arquivo JSON
     * @param ctx
     * @param datatype
     * @param overwrite
     * @throws ParseException
     */

    private static void getDataFromHuawei(final Context ctx, final int datatype, final boolean overwrite) throws ParseException {

        // 1. Build the time range for the query: start time and end time.
        // Pega a data salva salva
        String dateString = "";
        String last_timestamp = SysUtils.loadInfoData(ctx, String.valueOf(datatype));

        // preenche 3 dias de dados a partir de hoje backward
        last_timestamp = "NOT_FOUND";

        // se tem a data salva, grava como sendo a data atual a ser extraída
        if (!last_timestamp.equals("NOT_FOUND")) {
            // pega somente a data
            dateString = last_timestamp;
        } else {
            // 9 dias atrás
            Date today = new Date();
            // 9 dias em miliseguntos 2147483648 max int -- pode melhorar para o histórico!
            long day_minutes_millis = 2073600000; // 24 dias //30 * (24*(60*(60*1000)));
            // hoja menos 9 dias
            long initial_date = today.getTime() - day_minutes_millis;
            Date idate = new Date(initial_date);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            dateString = dateFormat1.format(idate);
        }

        // transfoma a date em objeto
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        // Para debug vamos fixar uma data inicial
        //Date startDate = dateFormat.parse("2021-01-12 09:00:00");
        //Date endDate = dateFormat.parse("2021-01-12 10:05:00");

        Date startDate = dateFormat.parse(dateString);

        // calcula a diferença com a data de hoje
        Date endDate = new Date();

        int timeout = 0;

        if ((endTime != 0) && (startTime != 0)) {
            // o fim é o dia de hoje
            //long endTime = System.currentTimeMillis();
            // o ultimo dia extraido
            //long startTime = startDate.getTime(); // Check Data of the last 30 days

            HiHealthDataQuery hiHealthDataQuery = new HiHealthDataQuery(
                    datatype,
                    startTime, //<----- variable startTime used
                    endTime,//<----- variable endTime used
                    new HiHealthDataQueryOption()
            );

            HiHealthDataStore.execQuery(ctx, hiHealthDataQuery, timeout, new ResultCallback() {
                @Override
                public void onResult(int resultCode, Object data) {

                    if (data != null) {
                        Log.i(TAG, "query not null");
                        List dataList = (List) data;
                        long lastDay = 0;

                        // verifica se teve sucesso
                        if ((resultCode == 0) && (dataList.size() > 0)) {
                            long last_day = 0;
                            logger("Sucesso lendo dados do HMS core. Tipo:" + datatype + "\n");
                            //stringBuffer.append("resultCode is ").append(String.valueOf(resultCode)).append(";(");
                            logger("Número de registros: " + dataList.size());

                            // Preenche o databuffer
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            if (dataList.size() > 0) {
                                for (Object obj : dataList) {
                                    HuaweiDataClass hdata = new HuaweiDataClass();
                                    // Passsos ou distância
                                    if (datatype == HiHealthPointType.DATA_POINT_STEP_SUM) {
                                        HiHealthPointData hiHealthDatas = (HiHealthPointData) obj;
                                        hdata.timestamp = new Date(hiHealthDatas.getStartTime());
                                        hdata.num_steps = hiHealthDatas.getValue();
                                    }
                                    if (datatype == HiHealthSetType.DATA_SET_CORE_SLEEP) {
                                        HiHealthSetData hiHealthDatas = (HiHealthSetData) obj;
                                        hdata.timestamp = new Date(hiHealthDatas.getStartTime());
                                        Map map = hiHealthDatas.getMap();
                                        for (Object key : map.keySet()) {
                                            Object value = map.get(key);
                                            ;
                                            int ikey = (int) key;
                                            switch (ikey) {
                                                case HiHealthSetType.CONTENT_SLEEP_NIGHT_SLEEP_AMOUNT:
                                                    hdata.total_sleep = (int) Float.parseFloat(value.toString());
                                                    break;
                                                case HiHealthSetType.CONTENT_SLEEP_REM:
                                                    hdata.total_sleep_rem = (int) Float.parseFloat(value.toString());
                                                    break;
                                                case HiHealthSetType.CONTENT_SLEEP_DEEP:
                                                    hdata.total_sleep_deep = (int) Float.parseFloat(value.toString());
                                                    break;
                                                case HiHealthSetType.CONTENT_SLEEP_LIGHT:
                                                    hdata.total_sleep_ligh = (int) Float.parseFloat(value.toString());
                                                    break;
                                                case HiHealthSetType.CONTENT_SLEEP_WHOLE_DAY_AMOUNT:
                                                    hdata.total_sleep_day = (int) Float.parseFloat(value.toString());
                                                    break;
                                                case HiHealthSetType.CONTENT_SLEEP_BED_TIME:
                                                    hdata.sleep_bed_timestamp = new Date(Long.parseLong(value.toString()));
                                                    break;
                                                case HiHealthSetType.CONTENT_SLEEP_DEEP_CONTINUITY:
                                                    hdata.max_deep_sleep_time = (int) Float.parseFloat(value.toString());
                                                    break;
                                                case HiHealthSetType.CONTENT_SLEEP_RISE_TIME:
                                                    hdata.sleep_rise_timestamp = new Date(Long.parseLong(value.toString()));
                                                    break;
                                                case HiHealthSetType.CONTENT_SLEEP_AWAKE_TIMES:
                                                    hdata.sleep_awake_times = (int) Float.parseFloat(value.toString());
                                                    break;
                                                case HiHealthSetType.CONTENT_SLEEP_SCORE:
                                                    hdata.sleep_score = (int) Float.parseFloat(value.toString());
                                                    break;
                                                default:
                                                    throw new IllegalStateException("Unexpected value: " + ikey);
                                            }
                                        }
                                    }
                                    if (datatype == HiHealthSetType.DATA_SET_HEART) {
                                        HiHealthSetData hiHealthDatas = (HiHealthSetData) obj;
                                        hdata.timestamp = new Date(hiHealthDatas.getStartTime());
                                        Map map = hiHealthDatas.getMap();
                                        for (Object key : map.keySet()) {
                                            Object value = map.get(key);
                                            ;
                                            int ikey = (int) key;
                                            switch (ikey) {
                                                case HiHealthSetType.CONTENT_HEART_RATE_MAX:
                                                    hdata.max_heart_rate = (int) Float.parseFloat(value.toString());
                                                    break;
                                                case HiHealthSetType.CONTENT_HEART_RATE_MIN:
                                                    hdata.min_heart_rate = (int) Float.parseFloat(value.toString());
                                                    break;
                                                case HiHealthSetType.CONTENT_HEART_RATE_REST:
                                                    hdata.rest_heart_rate = (int) Float.parseFloat(value.toString());
                                                    break;
                                                default:
                                                    throw new IllegalStateException("Unexpected value: " + ikey);
                                            }
                                        }
                                    }

                                    // se já tem dado na lista
                                    boolean insert_data = false;
                                    int data_index = 0;
                                    if (DataBuffer.size() > 0) {
                                        // procura a data na lista
                                        for (int index = 0; index < DataBuffer.size(); index++) {
                                            HuaweiDataClass ho = DataBuffer.get(index);
                                            if (ho.timestamp.getTime() == hdata.timestamp.getTime()) {
                                                // já tem a data, marca que é para inserir
                                                insert_data = true;
                                                data_index = index;
                                                break;
                                            }
                                        }

                                        // Se for para inserir
                                        if (insert_data) {
                                            // Inserir batimentos
                                            if (datatype == HiHealthSetType.DATA_SET_HEART) {
                                                HuaweiDataClass ho = DataBuffer.get(data_index);
                                                ho.min_heart_rate = hdata.min_heart_rate;
                                                ho.max_heart_rate = hdata.max_heart_rate;
                                                ho.rest_heart_rate = hdata.rest_heart_rate;
                                            }
                                            // Inserir sono
                                            if (datatype == HiHealthSetType.DATA_SET_CORE_SLEEP) {
                                                HuaweiDataClass ho = DataBuffer.get(data_index);
                                                ho.total_sleep_day = hdata.total_sleep_day;
                                                ho.total_sleep = hdata.total_sleep;
                                                ho.total_sleep_ligh = hdata.total_sleep_ligh;
                                                ho.total_sleep_deep = hdata.total_sleep_deep;
                                                ho.total_sleep_rem = hdata.total_sleep_rem;
                                                ho.sleep_awake_times = hdata.sleep_awake_times;
                                                ho.max_deep_sleep_time = hdata.max_deep_sleep_time;
                                                ho.sleep_rise_timestamp = hdata.sleep_rise_timestamp;
                                                ho.sleep_bed_timestamp = hdata.sleep_bed_timestamp;
                                                ho.sleep_score = hdata.sleep_score;
                                            }
                                            // Inserir passos
                                            if (datatype == HiHealthPointType.DATA_POINT_STEP_SUM) {
                                                HuaweiDataClass ho = DataBuffer.get(data_index);
                                                ho.num_steps = hdata.num_steps;
                                            }

                                        } else {
                                            // adiciona o registro pq não achou a data
                                            DataBuffer.add(hdata);
                                        }
                                    } else {
                                        // adiciona o primeiro registro
                                        DataBuffer.add(hdata);
                                    }
                                }
                            } else {
                                logger("Sem dados");
                            }

                            Collections.sort(DataBuffer, new Comparator<HuaweiDataClass>() {
                                @Override
                                public int compare(HuaweiDataClass z1, HuaweiDataClass z2) {
                                    if (z1.timestamp.getTime() > z2.timestamp.getTime())
                                        return 1;
                                    if (z1.timestamp.getTime() < z2.timestamp.getTime())
                                        return -1;
                                    return 0;
                                }
                            });

                            // DEBUG
                            //Gson gson = new Gson();
                            //String json = gson.toJson(DataBuffer);
                            //logger(json);

                            // salva a última data obtida
                            //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            //String lastts = sdf.format(new Date(lastDay));
                            //SysUtils.saveInfoData(ctx, last_date, String.valueOf(datatype));

                            if (DataBuffer.size() > DataCount) {
                                DataCount = DataBuffer.size();
                            }

                            logger(SPLIT);
                        } else {
                            logger("Sem dados do tipo: " + String.valueOf(datatype));
                        }
                    }
                }
            });
        }
    }

    public static BarGraphSeries<DataPoint> fillGraphData() {
        try {
            BarGraphSeries <DataPoint> series = new BarGraphSeries <>();

            // faz loopdos dados
            for (int index = 0; index < DataBuffer.size() - 1; index++) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                //Date date = DataBuffer.get(index).timestamp;
                series.appendData(new DataPoint(index, DataBuffer.get(index).sleep_score), true, 256);
            }
            // styling
            series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
                @Override
                public int get(DataPoint data) {
                    // determinar as faixas
                    if (data.getY() >= 80) // Green
                        return Color.rgb(0, 165, 0);;
                    if ((data.getY() >= 70) || (data.getY() < 80)) // Yellow
                        return Color.rgb(200, 200, 0);
                    if ((data.getY() >= 60) || (data.getY() < 70)) // Laranja
                        return Color.rgb(255, 165, 0);
                    if ((data.getY() >= 60) || (data.getY() < 70)) // Vermelhor
                        return Color.rgb(250, 0, 0);
                    return Color.WHITE;
                }
            });
            series.setDrawValuesOnTop(true);
            series.setValuesOnTopColor(Color.BLUE);
            series.setTitle("Qualidade do sono");
            return series;
            //graph.addSeries(series);
        } catch (IllegalArgumentException e) {
            //Toast.makeText(, e.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }
}
