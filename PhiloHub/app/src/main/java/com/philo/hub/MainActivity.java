/*
 * Aplicativo para extrair dados do Huawei "Health"
 * SDK HiHealth kit
 * Copyright (c) Philo Care do Brasil Ltda. Todos direitos reservados
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2019. All rights reserved.
 */

package com.philo.hub;

import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;

import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.huawei.hihealth.device.HiHealthDeviceInfo;
import com.huawei.hihealth.listener.ResultCallback;
import com.huawei.hihealthkit.data.HiHealthData;
import com.huawei.hihealthkit.data.HiHealthSequenceData;
import com.huawei.hihealthkit.data.store.HiHealthDataStore;
import com.huawei.hihealthkit.data.store.HiRealTimeListener;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private static final int GET_GENDER = 2;

    private static final int GET_BIRTHDAY = 3;

    private static final int START_READING_HEARTRATE = 12;
    private static final int STOP_READING_HEARTRATE = 13;

    private TextView dataText;
    private TextView textConnection;
    private PopupWindow window;
    public int counter=0;

    // serviço que será executado em background
    //private GetDataService mGetDataService;

    private Context ctx;

    public MainActivity() {
    }

    public Context getCtx() {
        return ctx;
    }

    // Nome do canal de comunicação para mensagens no status bar
    public static final String CHANNEL_ID = "common";
    public static final String ACTION_SNOOZE = "snooze";
    public static final String ACTION_MONITOR = "monitoring_action";
    private Notification note;

    // controle de indexação dos arquivos (evita perda de dados)
    public static int index_save = 0;

    public static String user_id;

    // TextView for displaying operation information on the UI
    private TextView logInfoView;
    private TextView tv1;
    private TextView tv2;
    private TextView tv3;
    private TextView mTitleTextView;
    private Message msg;

    GraphView graph = null;

    //#####################################################################
    // Mensagens
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String s = msg.obj.toString();
//            Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();

            switch (msg.what) {
                case 1:
                    tv1.setText(s + "\n");
                    break;
            }
        }
    };

    private void sendMsg(String message, int what) {
        msg = Message.obtain();
        msg.what = what;
        msg.obj = message;
        mHandler.sendMessage(msg);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //dataText = findViewById(R.id.dataText);

        ctx = this;

        logInfoView = (TextView) findViewById(R.id.data_controller_log_info);
        logInfoView.setMovementMethod(ScrollingMovementMethod.getInstance());

        // Exibição de textos
        tv1 = (TextView) findViewById(R.id.tv1);
        //tv2 = (TextView) findViewById(R.id.tv2);
        //tv3 = (TextView) findViewById(R.id.tv3);

        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE}, 1);

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //popSplashWindow(findViewById(R.id.main_layout), R.layout.activity_splash);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        View main_view = (View)findViewById(R.id.main_layout);
                        main_view.setVisibility(View.VISIBLE);
                        //dismissSplashWindow();
                    }
                }, 2000);
            }
        }, 100);*/

        initView();

        /*
        Notificações no status bar
        */
        createNotificationChannel();

        // Verifica a permissão do aplicativo
        doGetGender(getCtx());

        /*mGetDataServiceIntent = new Intent(this, GetDataService.class);
        mGetDataServiceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
        ContextCompat.startForegroundService(this, mGetDataServiceIntent);*/

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_MONITOR);
        filter.addAction(ACTION_SNOOZE);

        // serviços de background
        //Intent mGetDataServiceIntent;
        //Intent serviceIntent;
        BcastReceiver receiver = new BcastReceiver();
        receiver.setContext(getCtx());
        registerReceiver(receiver, filter);

        // Controle de index files
        String str_index_save = SysUtils.loadInfoData(ctx, "index");

        if (str_index_save.equals(SysUtils.NOT_FOUND) || str_index_save.equals("")) {
            index_save = 0;
        } else {
            index_save = Integer.parseInt(str_index_save);
        }

        // por enquanto o identificador será o Mc address do bluetooth
        // user_id = "50:48:49:4C:4F:01";
        // 24-07-2020 - Robson
        // Por decisão do Douglas não vamos focar no serviço e o plicativo irá fazer a
        // extração de dados e exibição na tela
        // Cria o serviço "imortal" de busca de dados
        //mGetDataService = new GetDataService();
        //mGetDataService.setCtx(getCtx());
        //mGetDataServiceIntent = new Intent(getCtx(), mGetDataService.getClass());
        //startService(mGetDataServiceIntent);
        /*
        if (!isMyServiceRunning(mGetDataService.getClass())) {
            //startService(mGetDataServiceIntent);
        }
         */
        // local para salvar o código da pulseira selecionada
        user_id = SysUtils.getSavedDevice(this);

        if (user_id.equals("")) {
            // Não tem usuário ainda, não iniciliza o timer
        } else {
            // inicia o timer resposável pela extração de dados
            //startTimer();
        }
        // inicializa o controlador dos dados para disco
        //HuaweiDataColector.initDataController(getCtx(), logInfoView);
        HuaweiDataColectorToMemory.initDataController(getCtx(), logInfoView);

        // Coloca a data corrente para a busca
        Date currentTime = Calendar.getInstance().getTime();
        HuaweiDataColectorToMemory.startTime = currentTime.getTime();
        HuaweiDataColectorToMemory.endTime = currentTime.getTime();

        final Calendar myCalendar = Calendar.getInstance();

        final TextView textViewinitialdate= (TextView) findViewById(R.id.tvInitialDateSelected);
        final DatePickerDialog.OnDateSetListener initial_date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //updateLabel();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                String date = dateFormat.format(myCalendar.getTime());
                textViewinitialdate.setText(date);
                HuaweiDataColectorToMemory.startTime = myCalendar.getTime().getTime();
            }

        };

        final TextView textViewEnddate= (TextView) findViewById(R.id.tvEndDateSelected);
        final DatePickerDialog.OnDateSetListener end_date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                //updateLabel();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                String date = dateFormat.format(myCalendar.getTime());
                textViewEnddate.setText(date);
                HuaweiDataColectorToMemory.endTime = myCalendar.getTime().getTime();
            }

        };

        textViewinitialdate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(ctx, initial_date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        textViewEnddate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(ctx, end_date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        graph = (GraphView) findViewById(R.id.graph);

        // inicia o timer resposável pela extração de dados
        //startTimer();
    }

    private TimerTask timerTask;
    long oldTime=0;

    /**
    Inicializa o timer de extração de dados do Huawei Health
     */
    public void startTimer() {
        //set a new Timer
        /**
         Variáveis para o timer de extração de dados
         */
        Timer timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask(ctx);

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 5000, 500000); //
    }

    /**
     * Inicializa a tarefa (thread) que faz o serviço de extração e calculos
     */
    public void initializeTimerTask(Context ctx) {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  "+ (counter++));
                try {
                    // Mostra notificação de monitoramento
                    showMonitorNote();

                    // Verifica a permissão e conexão do aplicativo e exibe situação atual
                    doGetGender(getCtx());


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * Verifica se já possui ultima leitura (o registro salvo de HR)
     * Sem registro significa que será a primeira vez que está executando
     */
    /*private boolean getLastHRRead() {
        // verifica se o arquivo existe
        String res = SysUtils.getHRSavedDate(getCtx());
        if (res.equals(SysUtils.NOT_FOUND)) {
            return false;
        } else {
            return true;
        }
    }*/
    /**
     * Verifica se já possui ultima leitura (o registro salvo de Sleep)
     * Sem registro significa que será a primeira vez que está executando
     */
    /*private boolean getLastSleepRead() {
        // verifica se o arquivo existe
        String res = SysUtils.getSleepSavedDate(getCtx());
        if (res.equals(SysUtils.NOT_FOUND)) {
            return false;
        } else {
            return true;
        }
    }*/

    /**
     * Notificação de monitoramento
     */
    private void showMonitorNote() {
        Intent intent = new Intent(this, BcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 234324243, intent, 0);

        note = new Notification(this, MainActivity.class);
        note.send_note("Oi", "Philo Care Hub", "Estamos monitorando sua saúde!", R.drawable.ic_stat_philo, pendingIntent);
    }

    // 24-07-2020 - Robson
    // Por decisão do Douglas não vamos focar no serviço e o plicativo irá fazer a
    // extração de dados e exibição na tela
    /**
    Verifica se o serviço está rodadando
     */
    /*private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }*/

    //#############################################################################################
    // Funções executadas depois do oncreate
    //#############################################################################################
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume () {
        super.onResume();
        // usaremos a função getGender para verificar se estamos conseguido ler o Huawei
        doGetGender(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }


    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
        // 24-07-2020 - Robson
        // Por decisão do Douglas não vamos focar no serviço e o plicativo irá fazer a
        // extração de dados e exibição na tela
        //sendBroadcast(new Intent("RESTART"));
        //stopService(mGetDataServiceIntent);
    }


    /**
    Cria o canal de comunicação do aplicativo
    */
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
    Desativa a tela de splash
     */
    public void dismissSplashWindow() {
        if (window != null) {
            window.dismiss();
            window = null;

        }

    }

    /**
    Exibe a tela de splash
     */
    public void popSplashWindow(View parent, int windowRes) {
        if (window == null) {
            LayoutInflater lay = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert lay != null;
            View popView = lay.inflate(windowRes, null);
            popView.setAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_out));

            window = new PopupWindow(popView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, true);
            window.setOutsideTouchable(true);
            window.setFocusable(true);
            window.update();

            window.showAtLocation(parent, Gravity.CENTER_VERTICAL, 0, 0);
            window.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    //bStart = true;
                    //callRemoteScanDevice();
                    window = null;
                }
            });

            // Evento de toque na tela
            /*window.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    dismissSplashWindow();
                    return false;
                }
            });*/

        }
    }


    public String getAppVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Inicializa os componentes da UI
     */
    private void initView() {
        //mSwipeRefreshLayout = (SwipeRefreshLayout) super.findViewById(R.id.mian_swipeRefreshLayout);
        //mRecyclerView = (RecyclerView) super.findViewById(R.id.main_recylerlist);
        mTitleTextView = (TextView) super.findViewById(R.id.main_title);
        mTitleTextView.setText(getString(R.string.app_name) + " " + getAppVersion(getCtx()));
        //mInfoTextView = (TextView) super.findViewById(R.id.textViewInfo);
    }


    //#############################################################################################
    // AÇÕES DE BOTÕES
    //#############################################################################################

    /**
     * Get data from Huawei
     * @param view linked view
     * @throws ParseException
     * @throws IOException
     */
    public void readData(View view) throws ParseException, IOException {
        Button btn = (Button) findViewById(R.id.btnGetData);
        btn.setEnabled(false);
        // busca dados do Saúde e grava em disco
        //HuaweiDataColector.getHealthData(getCtx());
        HuaweiDataColectorToMemory.getHealthData(getCtx());
    }

    /**
    Envia um broadcast para o receiver
     */
    public void btnBroadcastClick(View view) {

        Intent intent = new Intent(this, BcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 234324243, intent, 0);

        note = new Notification(this, MainActivity.class);
        note.send_note("Oi", "Philo Care Hub", "Estamos monitorando sua saúde!", R.drawable.ic_stat_philo, pendingIntent);


        /*
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        assert alarmManager != null;
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + (5 * 1000), pendingIntent);
        Toast.makeText(this, "Alarm set in " + 5 + " seconds",
                Toast.LENGTH_LONG).show();
         */
    }

    /*
    Ativa a tela de configuração
    */
    public void btnSettingsClick(View view) {
        Intent intent = new Intent(
                MainActivity.this, SettingsActivity.class
        );
        startActivity(intent);
    }

    /*
    Ativa a tela de autenticação
    */
    public void btnAuthClick(View view) {
        Intent intent = new Intent(
                MainActivity.this, HuaweiAuthActivity.class
        );
        startActivity(intent);
    }

    public void sendPost(View view) {
        // DEBUG - Faz gráfico
        Button btn1 = (Button) findViewById(R.id.btnGetData);
        Button btn2 = (Button) findViewById(R.id.btnPost);
        btn2.setEnabled(false);
        BarGraphSeries<DataPoint> ser = null;
        // Limpa o gráfico
        graph.removeAllSeries();
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMaxX(HuaweiDataColectorToMemory.DataCount);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMaxY(100);
        ser = HuaweiDataColectorToMemory.fillGraphData();
        if (ser != null) {
            graph.addSeries(ser);
        }
        //postAndGetJsonWebToken();
        btn2.setEnabled(true);
        btn1.setEnabled(true);
    }

    /**
     * #############################################################################################
     * Envia dados para a servidor na nuvem
     * EM DESENVOLVIMENTO
     * #############################################################################################
     */
    private void postAndGetJsonWebToken() {
        //HttpURLConnection conn = null;

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    //Your code goes here
                    String response = "";

                    // Dados para autenticação
                    StringBuilder sbParams = new StringBuilder();
                    sbParams.append("{");
                    sbParams.append("\"passwd\"");
                    sbParams.append(":");
                    sbParams.append("\"123456aA\"");
                    sbParams.append(",");
                    sbParams.append("\"email\"");
                    sbParams.append(":");
                    sbParams.append("\"philo\"");
                    sbParams.append("}@");

                    // Dados extraidos da pulseira
                    // Lê um arquivo e envia no post
                    String[] samples = SysUtils.readTxtFile();

                    if (samples == null) {
                        sendMsg("Nada para enviar", 1);
                        return;
                    }

                    sbParams.append(samples[1]);


                    try{
                        //String requestURL = "https://www.philocare.com/engine/devicedatareceive_post.php";
                        String requestURL = "https://philo.solutions/engine/huawei_devicedatareceive_post_hihealth.php";

                        URL url;

                        try {
                            url = new URL(requestURL);

                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setReadTimeout(15000);
                            conn.setConnectTimeout(15000);
                            conn.setRequestMethod("POST");
                            conn.setDoInput(true);
                            conn.setDoOutput(true);


                            OutputStream os = conn.getOutputStream();
                            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(os, "UTF-8"));
                            writer.write(sbParams.toString());

                            writer.flush();
                            writer.close();
                            os.close();
                            int responseCode=conn.getResponseCode();

                            if (responseCode == HttpsURLConnection.HTTP_OK) {
                                String line;
                                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                                while ((line=br.readLine()) != null) {
                                    response+=line;
                                }
                            }
                            else {
                                response="Falha de recepção. Erro: " + responseCode;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        //return response;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String millisInString  = dateFormat.format(new Date());

                    sendMsg(millisInString + ": " + response, 1);
                    // apaga o arquivo enviado
                    if (response.contains("processed")) {
                        //SysUtils.deleteFile(samples[0]);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        thread.start();

    }


    /*
    ################################################################################################
    Métodos de acesso ao "Saúde" da Huawei
    ################################################################################################
     */

    /**
     * Developer Alliance Code:getGender
     *
     * @param context context
     */
    private void doGetGender(Context context) {
        HiHealthDataStore.getGender(context, new ResultCallback() {
            @Override
            public void onResult(int errorCode, Object gender) {
                Log.i(TAG, "call doGetGender() resultCode is " + errorCode);
                Log.i(TAG, "call doGetGender() gender is " + gender);
                if (gender != null) {
                    Message message = Message.obtain();
                    message.what = GET_GENDER;
                    message.obj = gender;
                    String res = String.valueOf(gender);
                    if (res.equals("failed")) {
                        sendMsg(getString(R.string.fail_auth), 1);
                    } else {
                        sendMsg(getString(R.string.connected), 1);
                    }
                } else {
                    sendMsg(getString(R.string.fail_huawei), 1);
                }
            }
        });
    }

    /**
     * Developer Alliance Code:getBirthday
     *
     * @param context context
     */
    private void doGetBirthday(Context context) {
        HiHealthDataStore.getBirthday(context, new ResultCallback() {
            @Override
            public void onResult(int resultCode, Object birthday) {
                Log.i(TAG, "call doGetBirthday() resultCode is " + resultCode);
                Log.i(TAG, "call doGetBirthday() birthday is " + birthday);
                if (birthday != null) {
                    // For example, "1978-05-20" would return 19780520
                    Message message = Message.obtain();
                    message.what = GET_BIRTHDAY;
                    message.obj = birthday;
                }
            }
        });
    }

    /**
     * Developer Alliance Code:startReadingHeartRate
     *
     * @param context context
     */
    private void doStartReadingHeartRate(Context context) {
        HiHealthDataStore.startReadingHeartRate(context, new HiRealTimeListener() {
            @Override
            public void onResult(int resultCode) {
                Log.i(TAG, "call doStartReadingHeartRate() resultCode is " + resultCode);
                Message message = Message.obtain();
                message.what = START_READING_HEARTRATE;
                message.obj = resultCode;
                //demoHandler.sendMessage(message);
            }

            @Override
            public void onChange(int resultCode, String data) {
                Log.i(TAG, "call doStartReadingHeartRate() resultCode is " + resultCode);
                Log.i(TAG, "call doStartReadingHeartRate() value is " + data);
                Message message = Message.obtain();
                message.what = START_READING_HEARTRATE;
                message.obj = data;
                //demoHandler.sendMessage(message);
            }
        });
    }

    /**
     * Developer Alliance Code:stopReadingHeartRate
     *
     * @param context context
     */
    private void doStopReadingHeartRate(Context context) {
        HiHealthDataStore.stopReadingHeartRate(context, new HiRealTimeListener() {
            @Override
            public void onResult(int resultCode) {
                Log.i(TAG, "call doStopReadingHeartRate() resultCode is " + resultCode);
                Message message = Message.obtain();
                message.what = STOP_READING_HEARTRATE;
                message.obj = resultCode;
                //demoHandler.sendMessage(message);
            }

            @Override
            public void onChange(int resultCode, String data) {
                Log.i(TAG, "call doStopReadingHeartRate() resultCode is " + resultCode);
                Log.i(TAG, "call doStopReadingHeartRate() value is " + data);
                Message message = Message.obtain();
                message.what = STOP_READING_HEARTRATE;
                message.obj = data;
                //demoHandler.sendMessage(message);
            }
        });
    }
}
