/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
 */

package com.philo.hub;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import static com.philo.hub.MainActivity.CHANNEL_ID;

public class GetDataService extends Service {

    private Notification note;

    public int counter=0;

    private Context mCtx;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("HERE", "here I am!");
    }

    public void setCtx(Context ctx) {
        mCtx = ctx;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        String input = intent.getStringExtra("inputExtra");

        startTimer();
        // START_NOT_STICKY;
        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");

        sendBroadcast(new Intent("RESTART"));

        //Intent broadcastIntent = new Intent(this, BcastReceiver.class);
        //sendBroadcast(broadcastIntent);
        stoptimertask();
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime=0;
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 5000, 5000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  "+ (counter++));
                try {

                    // Do stuff that alters the content of my local SQLite Database
                    sendBroadcast(new Intent("SOME_ACTION"));

                    /*Intent intent_next = new Intent(GetDataService.this, BcastReceiver.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
                            GetDataService.this, 234324243, intent_next, 0);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
